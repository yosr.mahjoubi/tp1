package com.tn.esprit.tp.log4j;

import org.apache.log4j.Logger;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.UUID.randomUUID;

@Service
public class OrganizationService {
    private static final Logger logger = Logger.getLogger(OrganizationService.class);

    @EventListener(ApplicationReadyEvent.class)
    public void initOrganizationAndStudents() {
        Student yosr = new Student(randomUUID().toString(), "Yosr", "Mahjoubi", 25, Student.Gender.FEMALE);
        Student maryem = new Student(randomUUID().toString(), "Maryem", "Mahjoubi", 24, Student.Gender.FEMALE);
        Student taha = new Student(randomUUID().toString(), "Taha", "Jelassi", 24, Student.Gender.FEMALE);
        Organization organization = new Organization(randomUUID().toString(), "ESPRIT");
        organization.addStudents(asList(yosr, maryem, taha));

        List<Student> studentByGender = organization.findStudentByGender(Student.Gender.MALE);
        if (studentByGender.isEmpty()) {
            logger.error("LIST WAS EMPTY");
        }
    }
}
