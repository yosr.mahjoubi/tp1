package com.tn.esprit.tp.log4j;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Organization {
    private String id;
    private String name;
    private List<Student> students = new ArrayList<>();

    public Organization(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student e) {
        this.students.add(e);
    }

    public void removeStudent(Student e) {
        this.students.remove(e);
    }

    public List<Student> findStudentByGender(Student.Gender gender) {
        return this.students
                .stream()
                .filter(student -> student.getGender() == gender)
                .sorted(Comparator.comparing(Student::getAge))
                .collect(toList());
    }

    public void addStudents(List<Student> students) {
        this.students.addAll(students);
    }
}
