package com.tn.esprit.tp.log4j;

public class Student {
    private String id;
    private String firstName;
    private String lastName;
    private int age;
    private Gender gender;

    public Student(String id, String firstName, String lastName, int age, Gender gender) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
    }

    public enum Gender {
        MALE, FEMALE
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }
}
